<b>Price Selector Web App </b> <br>


**Video Link** : https://drive.google.com/file/d/1GQoiwQCZWT7DZ2LBwc9Jk2gDtbjTENX8/view
This is a simple web application that allows users to select a price range and view associated pricing options. The application also provides a way to submit order details through a modal form and dynamically load images from the Unsplash API based on user interactions.

**Table of Contents**

**Introduction** <br>
Technologies Used  <br>
How to Use <br> 
Features <br>
Introduction  <br>

This web application presents three pricing options to the user, allowing them to select a preferred price range. The selected price range is highlighted, giving visual feedback to the user. Additionally, the application loads images from Unsplash and allows the user to load more images as they scroll. There's also a modal for users to submit their order details.

**Technologies Used**

HTML <br>
CSS<br>
Bootstrap<br>
JavaScript<br>
Unsplash API<br>
How to Use

Open the HTML file in a web browser.
You will see three pricing options displayed on the page.
Use the price slider to select a price range (0-10, 10-20, 20-30). The pricing options will be highlighted accordingly.
Click the "Select" button for any pricing option to open the order details modal.
In the modal, enter your name, email, and any comments related to the order.
Click "Submit" to submit the order details.

**Features**

**Pricing Options**: 
Three pricing options are displayed, and the selected price range is visually highlighted.<br>
**Image Loading:** Images are fetched from Unsplash and displayed on the page. More images are loaded as the user scrolls.<br>
**Order Details Modal:** Users can enter their name, email, and order comments in a modal, which can be submitted.<br> <br>
